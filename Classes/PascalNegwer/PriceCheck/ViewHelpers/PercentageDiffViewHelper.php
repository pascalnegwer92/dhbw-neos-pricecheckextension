<?php
namespace PascalNegwer\PriceCheck\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

use TYPO3\Flow\Annotations as Flow;

class PercentageDiffViewHelper extends AbstractViewHelper
{

    /**
     * @param float $numberA
     * @param float $numberB
     * @return bool|float
     */
    public function render($numberA, $numberB)
    {
        if ($numberA == 0) {
            return false;
        }
        return ($numberB * 100 / $numberA) - 100;
    }
}