<?php
namespace PascalNegwer\PriceCheck\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

use TYPO3\Flow\Annotations as Flow;

class DateDiffViewHelper extends AbstractViewHelper
{

    /**
     * @param \DateTime $dateA
     * @param \DateTime $dateB
     * @param string $format
     * @return string
     */
    public function render(\DateTime $dateA, \DateTime $dateB = null, $format = '%d')
    {
        if (empty($dateB)) {
            $dateB = new \DateTime('now', new \DateTimeZone('EUROPE/BERLIN'));
        }

        $interval = $dateB->diff($dateA);
        return $interval->format($format);
    }
}