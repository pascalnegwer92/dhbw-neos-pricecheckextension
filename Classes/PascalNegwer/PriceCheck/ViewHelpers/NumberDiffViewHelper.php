<?php
namespace PascalNegwer\PriceCheck\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

use TYPO3\Flow\Annotations as Flow;

class NumberDiffViewHelper extends AbstractViewHelper
{

    /**
     * @param $numberA
     * @param $numberB
     * @return float
     */
    public function render($numberA, $numberB)
    {
       return $numberB - $numberA;
    }
}