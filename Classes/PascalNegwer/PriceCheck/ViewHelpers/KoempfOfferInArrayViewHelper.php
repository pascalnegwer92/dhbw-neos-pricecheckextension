<?php
namespace PascalNegwer\PriceCheck\ViewHelpers;

use PascalNegwer\PriceCheck\Domain\Model\Offer;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

use TYPO3\Flow\Annotations as Flow;

class KoempfOfferInArrayViewHelper extends AbstractViewHelper
{

    /**
     * @param Offer[] $offers
     * @return float
     */
    public function render($offers)
    {
        $cheapestKoempfOffer = 0;
        foreach ($offers as $offer) {
            if ($offer->isKoempfOffer()) {
                /** @var Offer $cheapestKoempfOffer */
                if (empty($cheapestKoempfOffer) || $cheapestKoempfOffer->getPrice() > $offer->getPrice()) {
                    $cheapestKoempfOffer = $offer;
                }
            }
        }
        return empty($cheapestKoempfOffer) ? $cheapestKoempfOffer : $cheapestKoempfOffer->getPrice();
    }
}