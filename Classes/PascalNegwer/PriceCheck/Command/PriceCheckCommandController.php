<?php
namespace PascalNegwer\PriceCheck\Command;

use PascalNegwer\PriceCheck\Domain\Model\Job;
use PascalNegwer\PriceCheck\Domain\Model\PriceApi\ApiJob;

use TYPO3\Flow\Cli\CommandController;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class PriceCheckCommandController extends CommandController
{
    /**
     * @var \PascalNegwer\PriceCheck\Domain\Helper\ApiRequestHelper
     * @Flow\Inject()
     */
    protected $apiRequestHelper;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Helper\DatabaseHelper
     * @Flow\Inject()
     */
    protected $databaseHelper;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Helper\ImportHelper
     * @Flow\Inject()
     */
    protected $importHelper;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\PriceApi\ApiJobRepository
     * @Flow\Inject()
     */
    protected $apiJobRepository;

    /**
     * @Flow\InjectConfiguration()
     * @var array
     */
    protected $settings;

    /**
     * @return void
     */
    public function handleNewJobsCommand()
    {
        $this->apiRequestHelper->handleNewJobs();
    }

    /**
     * @return void
     */
    public function removeOutdatedDataCommand()
    {
        $this->databaseHelper->removeOutdatedData($this->settings['outputDaysPeriod']);
    }

    /**
     * Flushes out all the Data in the PriceCheck tables
     *
     * @return void
     */
    public function removeAllCommand()
    {
        $this->databaseHelper->removeAll();
    }

    /**
     * Should be a JSON-Decodeable File - Just for Importing local Files
     *
     * @param string $path The path of the file containing the Data as
     * @return void
     */
    public function importJobFileCommand($path)
    {
        $jobArray = json_decode(file_get_contents($path), true);

        $job = $this->simulateEanScan($jobArray); // Just for Testing with local files

        $this->importHelper->importJob($jobArray, $job);
    }

    /**
     * Just for Testing with local Files
     *
     * @param $jobArray
     * @return Job
     * @throws \TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function simulateEanScan($jobArray)
    {
        $job = new Job();
        $this->jobRepository->add($job);

        $apiJob = new ApiJob();
        $apiJob->setStatus('finished')
            ->setApiJobId($jobArray['job_id'])
            ->setJob($job);
        $this->apiJobRepository->add($apiJob);

        return $job;
    }
}