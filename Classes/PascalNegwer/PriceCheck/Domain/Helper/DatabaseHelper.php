<?php
namespace PascalNegwer\PriceCheck\Domain\Helper;

use PascalNegwer\PriceCheck\Domain\Model\Product;
use PascalNegwer\PriceCheck\Domain\Model\Offer;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class DatabaseHelper
{
    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\ProductRepository
     * @Flow\Inject()
     */
    protected $productRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\ShopRepository
     * @Flow\Inject()
     */
    protected $shopRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\OfferRepository
     * @Flow\Inject()
     */
    protected $offerRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\PriceApi\ApiJobRepository
     * @Flow\Inject()
     */
    protected $apiJobRepository;

    /**
     * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
     * @Flow\Inject()
     */
    protected $persistenceManager;

    /**
     * @Flow\InjectConfiguration()
     * @var array
     */
    protected $settings;

    /**
     * @return void
     */
    public function removeAll()
    {
        $this->jobRepository->removeAll();
        $this->productRepository->removeAll();
        $this->offerRepository->removeAll();
        $this->shopRepository->removeAll();
        $this->apiJobRepository->removeAll();
    }

    /**
     * @param $days
     * @throws \TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException
     * @return void
     */
    public function removeOutdatedData($days)
    {
        $outDatedJobs = $this->jobRepository->findOlderThanDays($days);
        foreach ($outDatedJobs as $jobKey => $job) {
            foreach ($job->getApiJobs() as $apiJob) {
                $this->apiJobRepository->remove($apiJob);
            }
            /** @var Product $product */
            foreach ($this->productRepository->findAll() as $productKey => $product) {
                /** @var Offer $offer */
                foreach ($product->getOffers() as $offerKey => $offer) {
                    if ($offer->getJob() == $job) {
                        $this->offerRepository->remove($offer);
                    }
                }
                if ($product->getOffers()->isEmpty()) {
                    $this->productRepository->remove($product);
                }
                $job->getProducts()->remove($productKey);
            }
            $this->jobRepository->remove($job);
        }
    }
}