<?php
namespace PascalNegwer\PriceCheck\Domain\Helper;

use PascalNegwer\PriceCheck\Domain\Model\Job;
use PascalNegwer\PriceCheck\Domain\Model\Product;
use PascalNegwer\PriceCheck\Domain\Model\Shop;
use PascalNegwer\PriceCheck\Domain\Model\Offer;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class ImportHelper
{
    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\PriceApi\ApiJobRepository
     * @Flow\Inject()
     */
    protected $apiJobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\OfferRepository
     * @Flow\Inject()
     */
    protected $offerRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\ProductRepository
     * @Flow\Inject()
     */
    protected $productRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\ShopRepository
     * @Flow\Inject()
     */
    protected $shopRepository;

    /**
     * @var array
     * @Flow\InjectConfiguration()
     */
    protected $settings;

    /**
     * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
     * @Flow\Inject()
     */
    protected $persistenceManager;

    /**
     * @param array $jobArray
     * @param Job $job
     * @throws \TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException
     * @return void
     */
    public function importJob(array $jobArray, Job $job)
    {
        $job->increaseFreeCredits($jobArray['free_credits'])
            ->increasePaidCredits($jobArray['paid_credits']);
        $this->jobRepository->update($job);

        foreach ($jobArray['products'] as $productArray) {
            if ($productArray['success']) {
                $product = $this->getProductByArray($productArray, $job);
                $this->createOffersByArray(
                    $product,
                    $job,
                    $productArray['source'],
                    !empty($productArray['offers']) ? $productArray['offers'] : array(array()));
            }
        }
    }

    /**
     * @param array $productArray
     * @param Job $job
     * @return Product $product
     */
    protected function getProductByArray(array $productArray, Job $job)
    {
        $product = $this->productRepository->findOneByEan($productArray['value']);
        if (empty($product)) {
            $product = new Product;
            $product->setEan($productArray['value']);
            $this->productRepository->add($product);
        }
        if (empty($product->getName() && isset($productArray['name']))) {
            $product->setName($productArray['name']);
        }
        if (empty($product->getBrandName() && isset($productArray['brand_name']))) {
            $product->setBrandName($productArray['brand_name']);
        }
        $product->setLastJob($job);
        $this->productRepository->update($product);
        return $product;
    }

    /**
     * @param Product $product
     * @param Job $job
     * @param string $source
     * @param array $offersArray
     * @return void
     * @throws \TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    protected function createOffersByArray(Product $product, Job $job, $source, array $offersArray)
    {
        foreach ($offersArray as $offerArray) {
            $price = isset($offerArray['price']) ? $offerArray['price'] : null;
            $shippingCosts = (isset($offerArray['shipping_costs']) && !empty(floatval($offerArray['shipping_costs']))) ? $offerArray['shipping_costs'] : null;

            if (!empty($price)) {
                $shop = $this->getShopByArray($offerArray);
            }
            $offer = $this->offerRepository->findOneBy(array(
                'product' => $product,
                'job' => $job,
                'shop' => !empty($shop) ? $shop : null,
                'price' => $price,
                'shippingCosts' => $shippingCosts
            ));
            if (!empty($offer)) {
                $offer->setSource($offer->getSource() . ' + ' . $source);
                $this->offerRepository->update($offer);
            } else {
                $offer = new Offer();
                $offer->setSource($source)
                    ->setJob($job)
                    ->setProduct($product)
                    ->setShop(!empty($shop) ? $shop : null)
                    ->setPrice($price)
                    ->setShippingCosts($shippingCosts)
                    ->setKoempfOffer($this->checkIfKoempfOffer($offer));
                $this->offerRepository->add($offer);
            }
        }
    }

    /**
     * @param array $offerArray
     * @return Shop
     * @throws \TYPO3\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    protected function getShopByArray(array $offerArray)
    {
        /** @var Shop $shop */
        $shop = null;
        $shopName = null;
        $shopUrl = null;

        if (!empty($offerArray['shop_name'])) {
            $shopName = $offerArray['shop_name'];
            $shop = $this->shopRepository->findOneByName($offerArray['shop_name']);
        }

        if (!empty($offerArray['shop_url'])
            && !in_array(
                $offerArray['shop_url'],
                $this->settings['blacklist']['shopUrls'])
        ) {
            $shopUrl = $offerArray['shop_url'];
            $shop = $this->shopRepository->findOneByUrl($shopUrl);
        }

        if (empty($shop)) {
            $shop = new Shop();
            $this->shopRepository->add($shop);
        }
        if (empty($shop->getName()) && !empty($shopName)) {
            $shop->setName($shopName);
        }
        if (empty($shop->getUrl()) && !empty($shopUrl)) {
            $shop->setUrl($shopUrl);
        }
        $this->shopRepository->update($shop);
        $this->persistenceManager->persistAll();
        return $shop;
    }

    /**
     * @param Offer $offer
     * @return bool
     */
    protected function checkIfKoempfOffer(Offer $offer)
    {
        $koempfStoreDomainNames = $this->settings['storeNames'];
        foreach ($koempfStoreDomainNames as $koempfStoreDomainName) {
            if (
            (
                stripos($offer->getShop()->getName(), $koempfStoreDomainName) !== false
                ||
                stripos($offer->getShop()->getUrl(), $koempfStoreDomainName) !== false
            )
            ) {
                return true;
            }
        }
        return false;
    }
}