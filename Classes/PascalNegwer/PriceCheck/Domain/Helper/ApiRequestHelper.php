<?php
namespace PascalNegwer\PriceCheck\Domain\Helper;

use PascalNegwer\PriceCheck\Domain\Model\Job;
use PascalNegwer\PriceCheck\Domain\Model\PriceApi\ApiJob;

use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\SwiftMailer\Message;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class ApiRequestHelper
{
    /**
     * @var ImportHelper
     * @Flow\Inject()
     */
    protected $importHelper;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\PriceApi\ApiJobRepository
     * @Flow\Inject()
     */
    protected $apiJobRepository;

    /**
     * @Flow\InjectConfiguration()
     * @var array
     */
    protected $settings;

    /**
     * @var PersistenceManagerInterface
     * @Flow\Inject()
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Http\Client\Browser
     */
    protected $browser;

    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Http\Client\CurlEngine
     */
    protected $browserRequestEngine;

    public function initializeObject()
    {
        $this->browser->setRequestEngine($this->browserRequestEngine);
    }

    /**
     * @param array $eanArray
     * @param array $sources
     * @return void
     */
    public function performEanScan(array $eanArray, array $sources = array())
    {
        $chunks = array_chunk($eanArray, (int)$this->settings['priceApi']['maxEansPerRequest']);
        foreach ($chunks as $eanChunk) {
            if (empty($job)) {
                $job = new Job();
                $this->jobRepository->add($job);
            }
            foreach (!empty($sources) ? $sources : $this->settings['priceApi']['sources'] as $source) {
                $responseArray = $this->sendRequest(
                    'jobs/',
                    array(),
                    array_merge(
                        array(
                            'values' => implode(PHP_EOL, $eanChunk),
                            'source' => $source
                        ),
                        $this->settings['priceApi']['bulkRequest']
                    )
                );
                if (!empty($responseArray['status']) && $responseArray['status'] === 'new') {
                    $apiJob = new ApiJob();
                    $apiJob->setApiJobId($responseArray['job_id'])
                        ->setStatus($responseArray['status'])
                        ->setJob($job);
                    $this->apiJobRepository->add($apiJob);
                }
            }
        }
    }

    /**
     * @return array $jobsArray
     * @throws \TYPO3\Flow\Http\Client\InfiniteRedirectionException
     */
    public function handleNewJobs()
    {
        $apiJobs = $this->apiJobRepository->findByStatus('new');
        $responses = array();

        foreach ($apiJobs as $apiJob) {
            $responseArray = $this->sendRequest('products/bulk/' . $apiJob->getApiJobId() . '/');

            if (!empty($responseArray['status']) && $responseArray['status'] === 'finished') {
                $apiJob->setStatus($responseArray['status']);
                $this->apiJobRepository->update($apiJob);
                $responses[] = array(
                    'apiJob' => $apiJob,
                    'responseArray' => $responseArray
                );
            }
        }
        $this->persistenceManager->persistAll();

        foreach ($responses as $response) {
            /** @var ApiJob $apiJob */
            $apiJob = $response['apiJob'];
            $this->importHelper->importJob($response['responseArray'], $apiJob->getJob());
        }
    }

    /**
     * @param $command
     * @param array $getParams
     * @param array $postParams
     * @return mixed|null
     */
    protected function sendRequest($command, array $getParams = array(), array $postParams = array())
    {
        try {
            $getParamsString = http_build_query(array_merge($this->settings['priceApi']['generalRequest'], $getParams));
            $response = $this->browser->request(
                $this->settings['priceApi']['baseUrl'] . $command . '?' . $getParamsString,
                empty($postParams) ? 'GET' : 'POST',
                $postParams,
                [],
                [],
                ''
            );
        } catch (\Exception $e) {
            $mail = new Message();
            $mail
                ->setFrom($this->settings['mail']['from'])
                ->setTo($this->settings['mail']['to'])
                ->setSubject('PascalNegwer.PriceCheck Exception thrown!')
                ->setBody($e->getMessage());
            $mail->send();
            return null;
        }
        return json_decode($response->getContent(), true);
    }
}