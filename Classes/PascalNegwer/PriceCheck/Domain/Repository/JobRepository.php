<?php
namespace PascalNegwer\PriceCheck\Domain\Repository;

use PascalNegwer\PriceCheck\Domain\Model\Job;
use PascalNegwer\PriceCheck\Domain\Model\Product;

use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\Repository;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 * @method Job findByIdentifier($identifier)
 */
class JobRepository extends Repository
{

    /**
     * @param $days
     * @return Job[]
     */
    public function findOlderThanDays($days)
    {
        $query = $this->createQuery();
        return $query->matching(
            $query->lessThanOrEqual(
                'date',
                date_sub(
                    new \DateTime(),
                    New \DateInterval('P' . $days . 'D')
                )
            )
        )
            ->execute();
    }

    /**
     * @param Product $product
     * @return Job
     */
    public function findRecentByProduct(Product $product)
    {
        $query = $this->createQuery();
        return $query->matching(
            $query->equals('offers.product', $product)
        )
            ->setOrderings(array('date' => QueryInterface::ORDER_DESCENDING))
            ->execute()
            ->getFirst();
    }
}
