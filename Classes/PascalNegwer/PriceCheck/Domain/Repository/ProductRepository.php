<?php
namespace PascalNegwer\PriceCheck\Domain\Repository;

use PascalNegwer\PriceCheck\Domain\Model\Product;

use TYPO3\Flow\Persistence\Repository;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 * @method Product[] findAll()
 * @method Product findOneByEan($ean)
 */
class ProductRepository extends Repository
{
    /**
     * @param int $days
     * @return Product[]
     */
    public function findOlderThanPeriod($days)
    {
        $query = $this->createQuery();
        return $query->matching(
            $query->lessThan(
                'offers.job.date',
                date_sub(
                    new \DateTime(),
                    New \DateInterval('P' . $days . 'D')
                )
            )
        )
            ->execute();
    }
}
