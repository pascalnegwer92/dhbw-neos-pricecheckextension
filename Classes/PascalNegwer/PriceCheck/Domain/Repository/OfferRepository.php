<?php
namespace PascalNegwer\PriceCheck\Domain\Repository;

use PascalNegwer\PriceCheck\Domain\Model\Product;
use PascalNegwer\PriceCheck\Domain\Model\Job;
use PascalNegwer\PriceCheck\Domain\Model\Offer;

use TYPO3\Flow\Persistence\Doctrine\Repository;
use TYPO3\Flow\Persistence\QueryInterface;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 * @method Offer findOneBy(array $criteria, array $orderBy = null)
 */
class OfferRepository extends Repository
{
    /**
     * @var JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var ProductRepository
     * @Flow\Inject()
     */
    protected $productRepository;

    /**
     * @param Product $product
     * @return Offer[]
     */
    public function findByProduct(Product $product)
    {
        $query = $this->createQuery();
        return $query->matching(
                $query->equals('product', $product)
        )
            ->setOrderings(array(
                'job.date' => QueryInterface::ORDER_DESCENDING,
                'price' => QueryInterface::ORDER_ASCENDING
            ))
            ->execute();
    }

    /**
     * @param Product $product
     * @param Job $job
     * @return Offer
     */
    public function findByProductAndJob(Product $product, Job $job)
    {
        $query = $this->createQuery();
        return $query->matching(
            $query->logicalAnd([
                $query->equals('product', $product),
                $query->equals('job', $job)
            ])
        )
            ->setOrderings(array(
                'price' => QueryInterface::ORDER_ASCENDING
            ))
            ->execute()
            ->getFirst();
    }

    /**
     * @param Product $product
     * @return Offer
     */
    public function findRecentKoempfOffer(Product $product)
    {
        $query = $this->createQuery();
        return $query->matching(
            $query->logicalAnd([
                $query->equals('product', $product),
                $query->equals('koempfOffer', true)
            ])
        )
            ->setOrderings(array(
                'job.date' => QueryInterface::ORDER_DESCENDING,
                'price' => QueryInterface::ORDER_ASCENDING
            ))
            ->execute()
            ->getFirst();
    }
}




