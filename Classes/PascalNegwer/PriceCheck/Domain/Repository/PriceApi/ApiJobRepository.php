<?php
namespace PascalNegwer\PriceCheck\Domain\Repository\PriceApi;

use PascalNegwer\PriceCheck\Domain\Model\PriceApi\ApiJob;

use TYPO3\Flow\Persistence\Repository;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 * @method ApiJob[] findByStatus($status)
 */
class ApiJobRepository extends Repository
{
}
