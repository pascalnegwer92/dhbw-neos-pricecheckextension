<?php
namespace PascalNegwer\PriceCheck\Domain\Repository;

use PascalNegwer\PriceCheck\Domain\Model\Shop;

use TYPO3\Flow\Persistence\Repository;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 * @method Shop findOneByUrl($url)
 */
class ShopRepository extends Repository
{
    /**
     * @param $name
     * @return Shop
     */
    public function findOneByName($name)
    {
        $query = $this->createQuery();

        return $query->matching(
            $query->like('name', $name)
        )
            ->execute()
            ->getFirst();
    }
}
