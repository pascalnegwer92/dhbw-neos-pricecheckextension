<?php
namespace PascalNegwer\PriceCheck\Domain\Model\PriceApi;

use PascalNegwer\PriceCheck\Domain\Model\Job;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Entity
 * @Flow\Lazy()
 */
class ApiJob
{
    /**
     * @var Job
     * @ORM\ManyToOne(inversedBy="apiJobs")
     */
    protected $job;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $apiJobId;

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return self
     */
    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiJobId()
    {
        return $this->apiJobId;
    }

    /**
     * @param string $apiJobId
     * @return self
     */
    public function setApiJobId($apiJobId)
    {
        $this->apiJobId = $apiJobId;
        return $this;
    }
}
