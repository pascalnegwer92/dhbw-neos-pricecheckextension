<?php
namespace PascalNegwer\PriceCheck\Domain\Model;

use PascalNegwer\PriceCheck\Domain\Model\PriceApi\ApiJob;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Entity
 */
class Job
{
    /**
     * @var ArrayCollection<\PascalNegwer\PriceCheck\Domain\Model\PriceApi\ApiJob>
     * @ORM\OneToMany(mappedBy="job")
     * @Flow\Lazy()
     */
    protected $apiJobs;

    /**
     * @var ArrayCollection<Offer>
     * @ORM\OneToMany(mappedBy="job")
     * @Flow\Lazy()
     */
    protected $offers;

    /**
     * @var ArrayCollection<Product>
     * @ORM\OneToMany(mappedBy="lastJob")
     * @Flow\Lazy()
     */
    protected $products;

    /**
     * @var int
     * @ORM\Column(nullable=true)
     */
    protected $freeCredits;

    /**
     * @var int
     * @ORM\Column(nullable=true)
     */
    protected $paidCredits;

    /**
     * automatically set in Constructor
     *
     * @var \DateTime
     */
    protected $date;


    public function __construct()
    {
        $this->apiJobs = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->date = new \DateTime('now', new \DateTimeZone('Europe/Berlin'));
    }

    /**
     * @return ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param ArrayCollection $offers
     * @return self
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
        return $this;
    }

    /**
     * @return int
     */
    public function getFreeCredits()
    {
        return $this->freeCredits;
    }

    /**
     * @param int $freeCredits
     * @return self
     */
    public function setFreeCredits($freeCredits)
    {
        $this->freeCredits = $freeCredits;
        return $this;
    }

    /**
     * @param int $freeCredits
     * @return self
     */
    public function increaseFreeCredits($freeCredits)
    {
        $this->freeCredits += $freeCredits;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaidCredits()
    {
        return $this->paidCredits;
    }

    /**
     * @param int $paidCredits
     * @return self
     */
    public function setPaidCredits($paidCredits)
    {
        $this->$paidCredits = $paidCredits;
        return $this;
    }

    /**
     * @param int $paidCredits
     * @return self
     */
    public function increasePaidCredits($paidCredits)
    {
        $this->paidCredits += $paidCredits;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return self
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return ApiJob[]
     */
    public function getApiJobs()
    {
        return $this->apiJobs;
    }

    /**
     * @param ArrayCollection $apiJobs
     */
    public function setApiJobs($apiJobs)
    {
        $this->apiJobs = $apiJobs;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }


}
