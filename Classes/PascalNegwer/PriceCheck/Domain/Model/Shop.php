<?php
namespace PascalNegwer\PriceCheck\Domain\Model;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Entity
 * @Flow\Lazy()
 */
class Shop
{
    /**
     * @var ArrayCollection<Offer>
     * @ORM\OneToMany(mappedBy="shop")
     * @Flow\Lazy()
     */
    protected $offers;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $url;


    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param ArrayCollection $offers
     * @return self
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
}
