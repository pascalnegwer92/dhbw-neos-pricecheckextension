<?php
namespace PascalNegwer\PriceCheck\Domain\Model;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use PascalNegwer\PriceCheck\Domain\Repository\OfferRepository;
use PascalNegwer\PriceCheck\Domain\Repository\ProductRepository;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\QueryResultInterface;

/**
 * @Flow\Entity
 */
class Product
{
    /**
     * @var ArrayCollection<Offer>
     * @ORM\OneToMany(mappedBy="product")
     * @Flow\Lazy()
     */
    protected $offers;

    /**
     * @var Job
     * @ORM\ManyToOne(inversedBy="products")
     */
    protected $lastJob;

    /**
     * @var string
     * @ORM\Column(unique=true)
     */
    protected $ean;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $brandName;

    /**
     * @var OfferRepository
     * @Flow\Inject()
     */
    protected $offerRepository;

    /**
     * @var Job
     * @Flow\Transient()
     */
    protected $cheapestKoempfOffer;

    /**
     * @var Job
     * @Flow\Transient()
     */
    protected $cheapestOffer;

    /**
     * @var Job
     * @Flow\Transient()
     */
    protected $secondCheapestOffer;


    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param ArrayCollection $offers
     * @return self
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
        return $this;
    }

    /**
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     * @return self
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrandName()
    {
        return $this->brandName;
    }

    /**
     * @param string $brandName
     * @return self
     */
    public function setBrandName($brandName)
    {
        $this->brandName = $brandName;
        return $this;
    }

    /**
     * @return Job
     */
    public function getLastJob()
    {
        return $this->lastJob;
    }

    /**
     * @param Job $lastJob
     */
    public function setLastJob($lastJob)
    {
        $this->lastJob = $lastJob;
    }

    public function getCheapestKoempfPrice()
    {
        if (empty($this->cheapestKoempfOffer))
        {
            $this->cheapestKoempfOffer = $this->offerRepository->findRecentKoempfOffer($this);
        }
        return !empty($this->cheapestKoempfOffer) ? $this->cheapestKoempfOffer->getPrice() : 0;
    }

    public function getCheapestOfferPrice()
    {
        if (empty($this->cheapestOffer))
        {
            $this->cheapestOffer = $this->offerRepository->findByProductAndJob($this, $this->getLastJob());
        }

        return !empty($this->cheapestOffer) ? $this->cheapestOffer->getPrice() : 0;
    }
}
