<?php
namespace PascalNegwer\PriceCheck\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Entity
 */
class Offer
{
    /**
     * @var Job
     * @ORM\ManyToOne(inversedBy="offers")
     * @ORM\Column(nullable=true)
     */
    protected $job;

    /**
     * @var Shop
     * @ORM\ManyToOne(inversedBy="offers")
     * @ORM\Column(nullable=true)
     * @Flow\Lazy()
     */
    protected $shop;

    /**
     * @var Product
     * @ORM\ManyToOne(inversedBy="offers")
     * @ORM\Column(nullable=true)
     */
    protected $product;

    /**
     * @var boolean
     */
    protected $koempfOffer;

    /**
     * @var float
     * @ORM\Column(nullable=true, type="decimal", scale=2)
     * @Flow\Lazy()
     */
    protected $price;

    /**
     * @var float
     * @ORM\Column(nullable=true, type="decimal", scale=2)
     */
    protected $shippingCosts;

    /**
     * @var string
     */
    protected $source;

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return self
     */
    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     * @return self
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return self
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getShippingCosts()
    {
        return $this->shippingCosts;
    }

    /**
     * @param float $shippingCosts
     * @return self
     */
    public function setShippingCosts($shippingCosts)
    {
        $this->shippingCosts = $shippingCosts;
        return $this;
    }

    /**
     * @return float
     */
    public function getInclusivePrice()
    {
        return $this->getPrice() + $this->getShippingCosts();
    }

    /**
     * @return boolean
     */
    public function isKoempfOffer()
    {
        return $this->koempfOffer;
    }

    /**
     * @param boolean $koempfOffer
     */
    public function setKoempfOffer($koempfOffer)
    {
        $this->koempfOffer = $koempfOffer;
    }


}
