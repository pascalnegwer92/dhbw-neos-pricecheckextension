<?php
namespace PascalNegwer\PriceCheck\Controller;

use PascalNegwer\PriceCheck\Domain\Model\Offer;
use PascalNegwer\PriceCheck\Domain\Model\Product;

use TYPO3\Flow\Error\Message;
use TYPO3\Flow\Mvc\Controller\ActionController;

use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class StandardController extends ActionController
{
    /**
     * @var \PascalNegwer\PriceCheck\Domain\Helper\ApiRequestHelper
     * @Flow\Inject()
     */
    protected $apiRequestHelper;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\PriceApi\ApiJobRepository
     * @Flow\Inject()
     */
    protected $apiJobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\JobRepository
     * @Flow\Inject()
     */
    protected $jobRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\OfferRepository
     * @Flow\Inject()
     */
    protected $offerRepository;

    /**
     * @var \PascalNegwer\PriceCheck\Domain\Repository\ProductRepository
     * @Flow\Inject()
     */
    protected $productRepository;

    /**
     * @return void
     */
    public function productListAction()
    {
        $products = $this->productRepository->findAll();
        $this->view->assign('products', $products);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function offerListAction(Product $product)
    {
        $this->view->assignMultiple(array(
            'product' => $product,
            'offers' => $this->offerRepository->findByProduct($product),
            'recentDate' => $this->jobRepository->findRecentByProduct($product)->getDate(),
            'recentKoempfOffer' => $this->offerRepository->findRecentKoempfOffer($product)
        ));
    }

    /**
     * @return void
     */
    public function showAddEansFormAction()
    {
        $this->view->assign('sources', $this->settings['priceApi']['sources']);
    }

    /**
     * @param array $input
     * @return void
     */
    public function addEansAction($input)
    {
        $eans = preg_split('/[,\n]/', $input['eans'], -1, PREG_SPLIT_NO_EMPTY);
        foreach ($eans as $key => $ean) {
            $eans[$key] = preg_replace('/[^0-9]/', '', $ean);
            if (empty($eans[$key]) || strlen($eans[$key]) < 8) {
                $this->addFlashMessage('EAN "' . $ean . '" ist ungültig!', '', Message::SEVERITY_ERROR);
                unset($eans[$key]);
            }
        }
        if (!empty($eans)) {
            $this->apiRequestHelper->performEanScan($eans, isset($input['sources']) ? $input['sources'] : array());
            $this->addFlashMessage(count($eans) . ' EANs werden nun gescannt. Dies kann einige Minuten in Anspruch nehmen.');
        }
        $this->redirect('productList');
    }

    /**
     * @return void
     */
    public function showRunningJobsAction()
    {
        $this->view->assign('apiJobs', $this->apiJobRepository->findByStatus('new'));
    }

    /**
     * @param array $eans
     * @return void
     */
    public function reScanAction($eans)
    {
        $this->apiRequestHelper->performEanScan($eans);
        $this->addFlashMessage(count($eans) . ' EANs werden nun gescannt. Dies kann einige Minuten in Anspruch nehmen.');
        $this->redirect('productList');
    }
}