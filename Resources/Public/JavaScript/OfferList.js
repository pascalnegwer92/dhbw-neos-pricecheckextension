$(document).ready(function () {
    $('.offer-quick-info').click(function () {
        $(this).next().toggleClass('hidden');
        $(this).find('.fa').toggleClass('fa-chevron-circle-down').toggleClass('fa-chevron-circle-up');
    });
});
