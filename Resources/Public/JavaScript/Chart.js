/*global define*/
/*jslint browser:true*/
define(
    [
        'jquery',
        'chart-js',
        'jquery.ui/widget'
    ],
    function ($, Chart) {
        'use strict';

        $.widget('Koempf_PriceCheck.Chart', {
            options: {
                width: 400,
                height: 400,
                data: [],
                chartOptions: []
            },

            _create: function () {
                var context,
                    chart;

                this.element.append(
                    '<div class="chart"><canvas width="' + this.options.width + '" height="' + this.options.height + '"></canvas></div>' +
                    '<div class="chartjs-tooltip"></div>'
                );

                Chart.defaults.global.pointHitDetectionRadius = 1;
                Chart.defaults.global.customTooltips = function (tooltip) {

                    var tooltipEl = $('.chartjs-tooltip');

                    if (!tooltip) {
                        tooltipEl.css({
                            opacity: 0
                        });
                        return;
                    }

                    tooltipEl.removeClass('above below');
                    tooltipEl.addClass(tooltip.yAlign);

                    tooltip.labels.sort(function(a, b){return parseFloat(a.split(' ')[0]) - parseFloat(b.split(' ')[0])});

                    var innerHtml = '';
                    for (var i = tooltip.labels.length - 1; i >= 0; i--) {
                        if (tooltip.labels[i])
                        {
                            innerHtml += [
                                '<div class="chartjs-tooltip-section">',
                                '	<span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
                                '</div>'
                            ].join('');
                        }

                    }
                    tooltipEl.html(innerHtml);

                    tooltipEl.css({
                        opacity: 1,
                        left: this.pageX,
                        top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px'//,
                    });
                };

                context = this.element.find('div.chart canvas')[0].getContext('2d');
                chart = new Chart(context);
                        chart.Line(this.options.data, this.options.chartOptions);

            }
        });
        return $.Koempf_PriceCheck.Chart;
    }
);