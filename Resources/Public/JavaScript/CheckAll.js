$(document).ready(function () {
    $('.check-all').click(function () {
        $(this).parents("table")
            .find("input:checkbox")
            .prop('checked', this.checked);
    });
});